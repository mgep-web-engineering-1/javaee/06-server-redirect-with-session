package edu.mondragon.webeng1.redirect_with_session.controller;

import java.io.IOException;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;

@WebServlet(name = "LoginController", urlPatterns = { "/login" })
public class LoginController extends HttpServlet {
    private static final long serialVersionUID = 1L;

    public LoginController() {
        super();
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession session = request.getSession();
        String username = request.getParameter("username");
        String password = request.getParameter("userpass");

        if (username == null || password == null || username.equals("") || password.equals("")) {
            // Guard clause (return to index with error)
            session.setAttribute("error", "Please fill all fields");
            response.sendRedirect(response.encodeRedirectURL("index.jsp"));
            return;
        }

        if (!username.equals(password)) {
            // Guard clause (return to index with error)
            session.setAttribute("error", "Wrong username or password");
            response.sendRedirect(response.encodeRedirectURL("index.jsp"));
            return;
        }

        session.setAttribute("username", username);
        response.sendRedirect(response.encodeRedirectURL("ok.jsp"));
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        doGet(request, response);
    }

}
