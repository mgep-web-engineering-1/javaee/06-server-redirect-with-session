# Servlet Redirect with Session

## Make it work

* Clone the repository.
* Open folder with VSCode.
* Right click and Run ```src > main > java > ... > Main.java```.
* Open http://localhost:8080 in the browser.

## Objective

It is not always advisable to send data in the URL, so there are other ways of storing user's data for future requests. One of them is using **Session**.

* In this example we will send a **username** and **password** to ```LoginController```.
* To simplify the logic, if both string are the same, the user will be authorized to enter the page.
  * We will redirect them to ```ok.jsp```.
  * We will store the username in the session so it can be displayed in the future.
  * Therefore, it can be used in several request once it is created.
* If **username** and **password** are not the same,
  * We will store an error in the session and we will redirect to ```index.jsp``` again.
  * There, we will print the error message.
  * If one of the fields is not filled, another error will be shown.

See that if we reload ```ok.jsp``` after username is in the session, the username will still be there. If we reload ```index.jsp``` the errors will be gone.

## Explaination

```LoginController``` will store data in the session before redirecting:

```java
// LoginController.java
...
session.setAttribute("username", username);
response.sendRedirect(response.encodeRedirectURL("ok.jsp"));
...
```

OK page will print the username in the session.

```jsp
<!-- ok.jsp -->
...
<h1>Hi <%= (String)session.getAttribute("username") %></h1>
...
```

Reloading the ok page will continue printing the username because the username is taken from the session and not from the request.

The variable in the session will be stored until server is restarted or session expires.

Reloading the index page after an error will not show the error again, because the JSP file removes it from the session after getting it.

```jsp
<!-- index.jsp -->
...
<%
...
session.setAttribute("error", null);
%>
```

## Exercise

* Create another JSP (```user.jsp```) that prints the user, so we can see that it is independent of the request.

* Create ```UserController``` that goes to that jsp, set a parameter. Depending on that parameter it will redirect or forward to ```user.jsp```.

  * ```/user?type=forward``` or ```/user?type=redirect```
  * Add a form in ```ok.jsp``` with 2 [radio-buttons](https://developer.mozilla.org/en-US/docs/Web/HTML/Element/input/radio) to chose the type and calls ```UserController```.

* Add an option to logout in ```user.jsp```. It will call a controller that will remove user parameter from session and will redirect to ```index.jsp```.

## Next and before

* Befoere [05-redirect-with-parameters](https://gitlab.com/mgep-web-engineering-1/javaee/05-redirect-with-parameters)
* Next [07-jsp-templates](https://gitlab.com/mgep-web-engineering-1/javaee/07-jsp-templates)